---
layout: markdown_page
title: "GitLab Direction"
---

- TOC
{:toc}

This page describes the direction and roadmap for GitLab. It's organized from
the short to the long term.

## Your contributions

GitLab's direction is determined by GitLab the company, and the code that is
sent by our [contributors](http://contributors.gitlab.com/). We continually
merge code to be released in the next version. Contributing is the best way to
get a feature you want included.

On [our issue tracker for CE][ce-issues] and [EE][ee-issues], many requests are
made for features and changes to GitLab. Issues with the [Accepting Merge
Requests] label are pre-approved as something we're willing to add to GitLab. Of
course, before any code is merged it still has to meet our [contribution
acceptance criteria].

[ce-issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues
[ee-issues]: https://gitlab.com/gitlab-org/gitlab-ee/issues
[Accepting Merge Requests]: https://gitlab.com/gitlab-org/gitlab-ce/issues?state=opened&label_name=Accepting+Merge+Requests
[contribution acceptance criteria]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria

## What our customers want

As a company, GitLab tries to make things that are useful for our customers as
well as ourselves. After all, GitLab is one of the biggest users of GitLab. If a
customer requests a feature, it carries extra weight. Due to our short release
cycle, we can ship simple feature requests, such as an API extension, within one
or two months.

## Previous releases

On our [release list page](/release-list/) you can find an overview of the most
important features of recent releases and links to the blog posts for each
release.

## Future releases

GitLab releases a new version [every single month on the 22nd]. Note that we
often move things around, do things that are not listed, and cancel things that
_are_ listed.

This page is always in draft, meaning some of the things here might not ever be
in GitLab. New premium features are indicated with "EE Premium" label. This is
our best estimate of what will be new premium features, but is in no way
definitive.

The list is an outline of **tentpole features** -- the most important features
of upcoming releases -- and doesn't include any contributions from volunteers
outside the company. This is not an authoritative list of upcoming releases - it
only reflects current [milestones](https://gitlab.com/groups/gitlab-org/milestones).

<%= direction %>

## Enterprise Editions

### Starter Features

Starter features are available to anyone with an Enterprise Edition subscription (Starter, Premium, Ultimate).

<%= wishlist["EE Starter"] %>

### Premium Features

Premium features will only be available to EE Premium (and in the future: Ultimate) subscribers.

<%= wishlist["EE Premium"] %>

### Ultimate Features

Ultimate features will only be available EE Ultimate subscribers.
Enterprise Edition Ultimate is not yet available.

<%= wishlist["EE Ultimate"] %>

[every single month on the 22nd]: /2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab

## Functional Areas

Below are features that represent the various functional areas we see GitLab going in. This list is not
prioritized. We invite everyone to join the discussion by clicking on the
items that are of interest to you. Feel free to comment, vote up or down any issue
or just follow the conversation. For GitLab sales, please add a link to the
account in Salesforce.com that has expressed interest in a wishlist feature. We
very much welcome contributions that implement any of these things.

### Chat Commands

<%= wishlist["chat commands"] %>

### Build and packaging

GitLab is the engine that powers many companies' software businesses so it is important to ensure it is as easy as possible to deploy, maintain, and stay up to date.

Today we have a mature and easy to use Omnibus based build system, which is the foundation for nearly all methods of deploying GitLab. It includes everything a customer needs to run GitLab all in a single package, and is great for installing on virtual machines or real hardware. We are committed to making our package easier to work with, highly available, as well as offering automated deployments on cloud providers like AWS.

We also want GitLab to be the best cloud native development tool, and offering a great cloud native deployment is a key part of that. We are focused on offering a flexible and scalable container based deployment on Kubernetes, by using enterprise grade Helm Charts.

#### GitLab High Availability
<%= wishlist["HA"] %>

#### Cloud Native Deployment
<%= wishlist["Cloud Native"] %>

#### Other Build Objectives
<%= wishlist["build"] %>

### CI / CD

We want to help developers get their code into production; providing convenience and confidence to the developer in an integrated way. CI/CD focuses on steps 6 through 9 of our [scope](#scope): Test (CI), part of Review (MR), Staging (CD), and part of Production (Chatops). When viewed through the CI/CD lens, we can group the scope into CI, CD, and things that are currently beyond any definition of CD.

![GitLab CI/CD Scope](/images/direction/cicd/revised-gitlab-ci-scope.svg)

We define our vision as “[Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)”: leveraging our integrated set of tools, it is simple to assist users in every phase of the development process, implementing automatic tasks that can be customized and refined to get the best fit for their needs.
Our idea is that the future will have “auto CI” to compile and test software based on best practices for the most common languages and frameworks, “auto review” with the help of automatic analysis tools like Code Climate, “auto deploy” based on Review Apps and incremental rollouts on Kubernetes clusters, and “auto metrics” to collect statistical data from all the previous steps in order to guarantee performances and optimization of the whole process.
Dependencies and artifacts will be first-class citizens in this world: everything must be fully reproducible at any given time, and fully connected as part of the great GitLab experience.

[Watch the video explaining our vision on Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c).

Many of the issues describe development of an n-tier web app, but could equally be applied to an iOS app, Ruby gem, static website, or other type of project.

See a slightly more complete rendering of an [example pipeline](complex-pipeline.svg).
{: .note}

<!--{: #sample .alert .alert-info}-->

#### Pipelines

<%= wishlist["pipeline"] %>

#### Build

GitLab CI provides an explicit `build` stage and the concept of build artifacts, but we might need to separate out the build artifacts from test artifacts. For example, you might want your test runner to create a JUnit-style output file which is available for external consumption, but not included in the build image sent to production. Creation of an explicit build aligns well with Docker where the result of the build stage is a Docker image which is stored in a registry and later pulled for testing and deployment.

<%= wishlist["ci-build"] %>

#### Test

<%= wishlist["test"] %>

#### Deploy

A key part of CD is being able to deploy. We currently have this ability via scripts in the `deploy` stage in `.gitlab-ci.yml`. We will go further.

<%= wishlist["deploy"] %>

#### Deliver

What's the difference between Deploy and Deliver? There's a big benefit to decoupling deployment of code from delivery of a feature, mostly using feature flags. Continuous integration helps improve the speed of development, but feature flags take it to another level, giving you the confidence to integrate code even more often while providing a gradual and granular method for delivery.

<%= wishlist["deliver"] %>

#### Monitor

See [Prometheus Monitoring](#prometheus-monitoring).
{: .note}

#### Misc

<%= wishlist["CI"] %>

### Code Review

<%= wishlist["code review"] %>

### Container Registry

<%= wishlist["container registry"] %>

### Moderation Tools

<%= wishlist["moderation"] %>

### Open Source Projects

<%= wishlist["open source"] %>

### Pages

<%= wishlist["pages"] %>

### Performance

<%= wishlist["performance"] %>

### Prometheus Monitoring

<%= wishlist["Prometheus"] %>

### Service Desk

<%= wishlist["service desk"] %>

### Team-first collaboration with issue boards

<%= wishlist["issue boards"] %>

### Usability

<%= wishlist["usability"] %>

### User management

<%= wishlist["user management"] %>

### Version Control for Everything

<%= wishlist["vcs for everything"] %>

### Wiki

<%= wishlist["wiki"] %>

### Workflow management with issues

<%= wishlist["issues"] %>

### Discussion: Issues and Merge Requests

#### Team-centered collaboration
- People create software in teams. Ideation, planning, execution, retrospection, and iteration happens in teams.
  - GitLab provides the tools and capabilities to solve these team-centered collaboration workflows.
- A team may be focused on a single line of business in an organization, or may be a cross-platform infrastructure group supporting many PnLs at the same time.
  - A team may typically work on one particular software codebase, or touch many code repositories day to day.
- GitLab leverages a hierarchical group structure (subgroups), enabling all types of organizational team structures to collaborate.
  - Issues, merge requests, milestones, boards, burndown charts, Gantt charts, and even chat integration are used by teams to effectively and efficiently deliver software business value to customers.
  - These tools are scoped at every group and subgroup level, providing maximum flexibility and configurability.
- GitLab is not constrained by separate code repositories or siloed issue tracking systems.
  - GitLab adapts to your team to facilitate and enhance the collaboration required to deliver quality software.
- Teams are often cross-functional and diverse in roles and responsibilities.
  - In addition to software engineers, there are product mangers, interaction and visual designers, projects managers, marketers, content creators, and salespeople.
  - GitLab is a tool that helps these additional roles collaborate and create digital content that may be an intermediary step in shipping software (e.g. design wireframes or mockups), or that may be part of the final product itself (e.g. published content on a marketing page that sells the software).
  - GitLab supports source control of rich digital content types used by these additional roles (e.g. images, video, design files, rich document files) elevating them into first-class citizen contributors.
  - GitLab even provides service desk, an integrated customer feedback system, closing the iteration loop, and ultimately incorporating the customer voice as part of your team.

#### Advanced code development toolset
- GitLab provides a set of code development tools for developers to construct, communicate, review, iterate, approve, and deliver source code to production.
- GitLab is a complete toolset, with web file editing, git branch management, merge requests for collaboration and approvals, and integrated CI/CD for software delivery to review and production environments. Completeness means that GitLab provides an end-to-end integrated experience, significantly reducing the entire development cycle time.
- GitLab provides the best web code browsing experience.
  - Users can browse through a codebase entirely within a web browser, navigating seamlessly between different files through searching and even clicking through linked objects in many different programming languages.
  - Code browsing is fully integrated into the merge request flow (for the purpose of introducing a change in the codebase) or for high-level informal review (e.g. a developer browsing through a new codebase when joining a new team).
- GitLab promotes code quality with tools to implement best practices.
  - GitLab integrates with static analysis tools to ensure that coding standards are maintained.
  - GitLab integrates with production logging tools, surfacing areas of code that frequently result in degraded user experiences, performance problems, or just obvious defects.
  - GitLab provides an approvals system, which allows developers a stage-gated process to review and sign off code authored by peers, and even make changes on behalf of them. Users select individuals as approvers. Approvers may also be based on role expertise (e.g. frontend engineer vs database engineer) or codebase maintainer (e.g. financial accounting system codebase maintainer). GitLab also tracks commit history, and therefore can automatically assign approvers for a given merge request, based on contributors' commit histories on a section of code, depending on the relevance of those contributions (i.e. amount, frequency, and recency of commits).
  - GitLab enables code maintainers to subscribe to sections of a codebase, notifying them when changes are proposed and subsequently merged, providing further vigilance in defending a quality codebase.
- GitLab is a code collaboration platform suited for teams working asychronously in time and space, or in a traditional co-located setting, or in any other other team structure variation in between.
- GitLab adapts to your custom development process (whether that be TDD, BDD, or others), and encourages a culture of responsibility and accountability amongst developers.

#### Enterprise software management
- Large enterprise organizations have many concurrent and interdependent initiatives as part of a larger roadmap, in various stages of planning, execution, and delivery.
- GitLab provides a single integrated platform to manage and track the many ongoing projects in your organization, allowing you to allocate resources accordingly. It allows for _strategic_ business planning.
- GitLab surfaces business risk by showing you which projects will likely be delayed, or are already behind schedule, allowing your organization to pro-actively take action, such as re-allocating resources or shuffling project timelines. It allows for _tactical_ execution.
- GitLab minimizes business risk by enabling your organization to configure and enforce custom enterprise-wide processes, such as stage-gated change control and approvals during different stages, including requirements analysis, scoping, development, testing, and release. GitLab thus provides your organization oversight, customization, and control, while still allowing your teams to execute quickly and efficiently.
- Enterprise organizations often also carry many legacy systems and data when coming to GitLab. GitLab eases the transition by providing integrations to popular tools such as JIRA, so that organizations can take a more methodical approach of incremental migration of data and functionalities.

#### Continuous process improvement
- GitLab is a complete software delivery platform, and so is well-equipped with the insight to help teams and even entire organizations to continuously improve their processes.
- GitLab surfaces actionable insight on an ongoing basis, from areas like cycle analytics and conversational development index, allowing your team to quickly adapt and improve, ultimately enabling you to ship better software, and more efficiently.
- As your organization scales up and business needs grow, GitLab shows you new features and higher tiers that become more important for continued success. You are able to seamlessly begin using these features and see how they impact your process metrics, with no additional complicated installation or configuration steps required.

## Moonshots

Moonshots are big hairy audacious goals that may take a long time to deliver.

<%= wishlist["moonshots"] %>

## Scope

[Our vision](#vision) is an integrated set of tools for application lifecycle management based on convention over configuration.

### Inside out scope

To achieve this we ship the following features in our Omnibus package:

![Lifecycle](handbook/sales/lifecycle.png)

1. **Idea** (Chat) => [Mattermost](http://www.mattermost.org/)
1. **Issue** (Tracker) => Issue Tracker
1. **Plan** (Board) => [Issue Boards](/features/issueboard/)
1. **Code** (IDE) => [Web editor](https://docs.gitlab.com/ce/user/project/repository/web_editor.html) and [web terminal](https://docs.gitlab.com/ce/ci/environments.html#web-terminals)
1. **Commit** (Repo) => GitLab Repositories
1. **Test** (CI) => [GitLab Continuous Integration](/features/gitlab-ci-cd/) and [Container Registry](https://about.gitlab.com/2016/05/23/gitlab-container-registry/)
1. **Review** (MR) => GitLab [Merge Requests](https://docs.gitlab.com/ce/user/project/merge_requests.html) and [Review Apps](/features/review-apps/)
1. **Staging** (CD) => [GitLab Deploy](https://docs.gitlab.com/ce/ci/environments.html) and [auto deploy](https://docs.gitlab.com/ce/ci/autodeploy/index.html)
1. **Production** (Chatops) => [Mattermost slash commands](https://docs.gitlab.com/ee/project_services/mattermost_slash_commands.html) and [Slack slash commands](https://docs.gitlab.com/ce/project_services/slack_slash_commands.html)
1. **Feedback** (Monitoring) => [Cycle Analytics](/features/cycle-analytics/) and [Prometheus](https://about.gitlab.com/2017/01/05/prometheus-and-gitlab/).

Also see our [demo](/handbook/product/i2p-demo/).

### Outside our scope

1. **Error monitoring** Sentry, Airbrake, Bugsnag
1. **Logging** [Fluentd](http://www.fluentd.org/architecture), ELK stack, Graylog, Splunk, [LogDNA](https://logdna.com/)
1. **Tracing** OpenTracing, [LightStep](http://lightstep.com/)
1. **Network** [Flannel](https://github.com/coreos/flannel/), Openflow, VMware NSX, Cisco ACI
1. **Network security** [Suricata](https://suricata-ids.org/), Nmap, rkhunter, Metasploit, Snort, OpenVAS, OSSEC
1. **Configuration management** although we do want to upload cookbooks, manifests, playbooks, and modules for respectively Chef, Puppet, Ansible, and Salt.
1. **Container configuration agents** [ContainerPilot](http://container-solutions.com/containerpilot-on-mantl/), [Orchestrator-agent](https://www.percona.com/blog/2016/04/13/orchestrator-agent-how-to-recover-a-mysql-database/)
1. **Distributed configuration stores** Consul, Etcd, Zookeeper, Vault
1. **Container Scheduler** although we do want to deploy to CloudFoundry, OpenStack, OpenShift, Kubernetes, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Operating System** Ubuntu, CentOS, [RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux), [CoreOS](https://coreos.com/), [Alpine Linux](https://alpinelinux.org/about/)

## Product Strategy

Today you can create an entire product successfully in GitLab, from idea to production. But you still need domain-specific knowledge to be able to set this up and then maintain, monitor and scale this application.

GitLab provides an [_integrated product_](#integrated-product) for [_teams of any size_](#teams-of-any-size) with [_any kind of projects_](#any-project) to move faster from idea to production, while giving you [_actionable feedback_](#actionable-feedback), and making shipping products _simple_.

GitLab's product is opinionated, but still allows you to use other tools if you
like to do so. GitLab plays well with others.

### Integrated Product

GitLab is an integrated set of tools for conversational development.
A single integrated tool has many advantages over separate components:

#### Integrated Authorization

GitLab does not require you to manage authorizations for each of
its tools. This means that you set permissions once and everyone in your organization has the right access to every component.

#### Integrated Authentication

You only have to login to one application. No extra steps needed.

#### Integrated interface

A single interface for all tools means that GitLab can always present the
relevant context and you're not losing information due to constant context switching.
Furthermore, if you're comfortable with one part of GitLab, you're comfortable
with all of GitLab, as it all builds on the same interface components.

#### Integrated installation

Running GitLab means that there are is only one product components to install, maintain, scale, backup and secure.

#### Integrated upgrades

Updating GitLab means that everything is guaranteed to work as it did before.
Maintaining separate components is often complicated by upgrades that change
or break integration points, essentially breaking your software delivery pipeline.
This will never happen with GitLab because everything is tested as an integrated whole.

#### Integrated data-store

GitLab uses a single data-store so you can get information about the whole software development life-cycle instead of parts of it.

#### Emergent benefits

An integrated product has unique, emergent benefits. Among them:

- It's no longer necessary to ask access to each separate tool.
Meaning everyone now is able to make use of all tools. Expect non-engineers to monitor deploys,
follow the development process and directly contribute to QA by reporting findings.
- Vastly improved cycle time. Constant context switching, re-authentication and lack of information
slows down teams immensely. It sounds obvious, but having everything you available at all times
makes for more efficient work.
- Tracking whether a change is being worked on, is live in an environment or is blocked no longer
requires detective work. It's available everywhere and accessible to everyone.

##### Conversational Development

Conversational development carries a conversation across functional groups through the application lifecycle management,
involving gatekeepers at every step. By providing relevant context,
a feature that is only possible with an integrated solution like GitLab, we can reduce cycle time, making it easier to diagnose problems and make decisions.

Concretely, conversational development in GitLab means that a change can be easily followed from inception
to the changes it made in performance and business metrics and feeding this information back to _all_ stakeholders _immediately_.

Effectively, this allows cross-functional teams to collaborate effectively.

##### Review Apps

Review apps are the future of change review. They allow you to review not just the code,
but the actual changes in a live environment. This means one no longer has to check out
code locally to verify changes in a development environment, but you simply click on
the environment link and try things out.

Only a tool that combines code review with CI/CD pipelines and integration with container schedulers (Kubernetes) is able
to quickly create and shut down review apps and make them part of the review process.

##### Cycle Analytics

Cycle analytics tell you how what your time to value, from idea to production is.
Only by having direct access to each step in the software development lifecyle,
GitLab can give actionable data on time to value.

This means you can see where you are lagging and make meaningful change to ship faster.

#### Idea to Production

GitLab contains all tools needed to bring any project from the ideation stage
up to running in production and giving feedback. This includes repositories,
issue tracking, CI, CD, monitoring, chat and more. GitLab focuses on lowering the threshold between each step, so that working on a project means focusing
on collaboration and not on learning new tools.

#### External Integrations

GitLab plays well with others. To allow everyone to contribute, it's important
that there is only one place where you'll have to look, even if you need to
use external tools that are not part of GitLab. For this reason, GitLab plays
well with others: Providing APIs for nearly everything you can do within GitLab
and powerful, simple authentication and authorization tools for external
integrations.

GitLab ships with built-in integrations to many popular applications.

### Any Project

GitLab is the tool for anyone working on software in any form.

#### Multi-repository projects

Software projects are often more than single repositories. GitLab will evolve from being focused around single repositories towards being able to accommodate software projects that consist of multiple repositories.

- [Nested Groups](https://gitlab.com/gitlab-org/gitlab-ce/issues/2772)

#### From scratch or legacy

Starting a project from scratch makes it easy to do everything through GitLab and well integrated. But existing / migrated / legacy projects should benefit from the same features that GitLab offers. GitLab will offer the tools to help you integrate, setup and improve your projects with the tools we have at offer. A concrete example is setting up review apps: everyone would benefit from this. GitLab will give you all the handles to set this up. It should not be required to dive deep in the documentation to discover features like this.

- [Auto Deploy](https://gitlab.com/gitlab-org/gitlab-ce/issues/23580)

#### Beyond code

Going from idea to production is not a matter of just code anymore. Modern products work with elaborate mockups and designs both in ideation, but also in production. Whether you’re working on assets in a game or working on the design of a new website, GitLab will allow you to collaborate on your work as a programmer would on their code.

- [Comment on images](https://gitlab.com/gitlab-org/gitlab-ce/issues/2641)

### Actionable Feedback

Deployments should never be fire and forget. GitLab will give you immediate feedback on every deployment on any scale. This means that GitLab can tell you whether performance has improved on the application level, but also whether business metrics have changed.

Concretely, we can split up monitoring and feedback efforts within GitLab in three distinct areas: execution (cycle analytics), business and system feedback.

#### Business feedback

With the power of monitoring and an integrated approach, we have the ability to do amazing things within GitLab. GitLab will be able to automatically test commits and versions through feature flags and A/B testing.

Business feedback exists on different levels:

* Short term: how does a certain change perform? Choose A/B based on data.
* Medium term: did a particular new feature change conversions, engagement
* Long term: how do larger efforts relate to changes in conversations, engagement, revenue

- [A/B Testing of branches](https://gitlab.com/gitlab-org/gitlab-ee/issues/117)

#### Application feedback

TODO

#### System feedback

We can now go beyond CI and CD. GitLab will able to tell you whether a change
improved performance or stability. Because it will have access to both
historical data on performance and code, it can show you the impact of any
particular change at any time.

System feedback happens over different time windows:

* Immediate: see whether changes influence availability and alert if they do
* Short-medium term: see whether changes influence system metrics and performance
* Medium-Long term: did a particular effort influence system status

- Implemented: [Performance Monitoring](https://docs.gitlab.com/ee/administration/monitoring/performance/introduction.html)
- [Status monitoring and feedback](https://gitlab.com/gitlab-org/gitlab-ce/issues/25555)
- [Feature monitoring](https://gitlab.com/gitlab-org/gitlab-ce/issues/24254)

#### Execution Feedback & Cycle Analytics

GitLab is able to speed up cycle time for any project.
To provide feedback on cycle time GitLab will continue to expand cycle
analytics so that it not only shows you what is slow, it’ll help you speed up
with concrete, clickable suggestions.

- [Cycle Speed Suggestions](https://gitlab.com/gitlab-org/gitlab-ce/issues/25281)

### Teams of any Size

GitLab is built to work for teams of any size. By yourself, it gets out of
your way and lets you quickly push you work and track your progress.

With a small team, you can easily communicate and quickly move through the
otherwise time-intensive steps of bringing a creation to production, with
infrastructure as code, a flexible issue tracker and very permissive defaults.

Large teams of thousands of collaborators working on complex projects can easily
manage permissions. Automatic and batch actions for getting rolling quickly,
up to fine-grained overrides to give certain users specific actions, temporary
if needed.

Teams working with external collaborators will find it easy and safe to allow
collaborators to work together with them in the same place as the rest of their
work, while securing what is not to be shared.

Open source initiatives can use GitLab to effectively collaborate with a
community, while having the moderation tools to keep the project clean and
focused.

### Enterprise Editions

GitLab comes in 3 editions, with Ultimate coming in the future:
* **Community Edition**: This edition is aimed at solo developers or teams that
do not need advanced enterprise features. It contains a complete stack with all
the tools developers needs to ship software.
* **Enterprise Edition Starter**: This edition contains features that are more
relevant for organizations that have more than 100 potential users. For example:
	* features for managers (reports, management tools at the group level,...),
	* features targeted at developers that have to work in multi-disciplinary
	teams (merge request approvals,...),
	* integrations with external tools.
* **Enterprise Edition Premium**: This edition contains features that are more
relevant for organizations that have more than 750 potential users. For example:
	* features for instance administrators
	* features for managers at the instance level (reporting, management tools,
	roles,...)
	* features to help teams that are spread around the world
	* features for people other than developers that help ship softwares (support,
	QA, legal,...)
* **Enterprise Edition Ultimate**: When released, this edition will contain
features that are more relevant for organizations that have more than 5000
potential users. For example:
	* compliances and certifications,
	* change management.

### Quarterly Objectives and Key Results (OKRs)

To make sure our goals are clearly defined and aligned throughout the organization, we make use of OKR's (Objective Key Results). Our [quarterly Objectives and Key Results](/okrs) are publicly viewable.

## Vision

From development teams to marketing organizations, everyone needs to collaborate on
digital content. Content should be open to suggestions by a wide number of
potential contributors. Open contribution can be achieved by using a mergeable
file format and distributed version control. The vision of GitLab is to **allow
everyone to collaborate on all digital content** so people can cooperate
effectively and achieve better results, faster.

Ideas flow though many stages before they are realized. An idea originates in a chat discussion, an issue is created, it is planned in a sprint, coded in an IDE, committed to
version control, tested by CI, code reviewed, deployed, checked and documented. Stitching all these stages of the application lifecycle together can be done in many different ways. You can have a marketplace of
proprietary apps from different suppliers or use a suite of products developed
in isolation. We believe that an **integrated set of tools for application lifecycle management based on convention over configuration** offers a superior user experience. The
advantage can be quoted from the [Wikipedia page for convention over
configuration](https://en.wikipedia.org/wiki/Convention_over_configuration):
"decrease the number of decisions that developers need to make, gaining
simplicity, and not necessarily losing flexibility". In GitLab you only have to
specify unconventional aspects of your workflow.
The happy path is **frictionless from idea to production**.

We admire other convention over configuration tools like [Ruby on
Rails](http://rubyonrails.org/) (that doctrine of which perfectly describe the [value of integrated systems](http://rubyonrails.org/doctrine#integrated-systems)), [Ember](http://emberjs.com/), and
[Heroku](https://www.heroku.com/), and strive to offer the same advantages for a
continuous delivery of software.

## Integrated product

We prefer to offer an integrated product instead of a network of services or offering plugins for the following reasons:

1. We think an integrated set of tools provides a better user experience than a modular approach, as detailed by [this article from Stratechery](https://stratechery.com/2013/clayton-christensen-got-wrong/).
1. The open source nature of GitLab ensures that that we can combine great open source products.
1. Everyone can contribute to create a feature set that is [more complete than other tools](https://about.gitlab.com/comparison/). We'll focus on making all the parts work well together to create a better user experience.
1. Because GitLab is open source the enhancements can become [part of
the codebase instead](http://doc.gitlab.com/ce/project_services/project_services.html) of being external. This ensures the automated tests for all
functionality are continually run, ensuring that additions keep working work. This is contrast to externally maintained plugins that might not be updated.
1. Having the enhancements as part of the codebase also
ensures GitLab can continue to evolve with its additions instead of being bound
to an API that is hard to change and that resists refactoring. Refactoring is essential to maintaining a codebase that is easy to contribute to.
1. Many people use GitLab on-premises, for such situations it is much easier to install one tool than installing and integrating many tools.
1. GitLab is used by many large organizations with complex purchasing processes, having to buy only one subscription simplifies their purchasing.
